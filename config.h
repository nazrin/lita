#include <stdlib.h>

#define S "\x1b["

#define RED     S "31m"
#define GREEN   S "32m"
#define YELLOW  S "33m"
#define BLUE    S "34m"
#define MAGENTA S "35m"
#define CYAN    S "36m"
#define GREY    S "37m"
#define BOLD    S "1m"
#define RESET   S "0m"

#define NUMBERS  S "38;5;140m"
#define SYMBOLS  S "38;5;70m"
#define MATH     S "38;5;73m"
#define PUNCT    S "38;5;67m"
#define BRACKETS S "38;5;33m"
#define QUOTES   S "38;5;35m"

struct Pair{
	const char* chars;
	const char* colour;
};

static const struct Pair colours[] = {
	{ "1234567890", NUMBERS },
	{ "#$&/\\@~|", SYMBOLS },
	{ "!+-?=%*^><", MATH },
	{ ".,:;_", PUNCT },
	{ "()[]{}", BRACKETS },
	{ "\"'`", QUOTES },
	{ NULL, NULL }
};

