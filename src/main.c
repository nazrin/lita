#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <locale.h>

#include "../config.h"

int main(int argc, char* argv[]){
	setlocale(LC_ALL, "");
	char line[257];
	while(fgets(line, sizeof(line), stdin)){
		int r = 0;
		while(r < 256){
			bool coloured = false;
			char c = line[r];
			if(!c)
				break;
			int ca = 0;
			while(colours[ca].chars != NULL){
				int ci = 0;
				while(1){
					char cic = colours[ca].chars[ci];
					if(!cic)
						break;
					if(cic == c){
						fputs(colours[ca].colour, stdout);
						coloured = true;
						goto end;
					}
					ci++;
				}
				ca++;
			}
end:
			putchar(c);
			if(coloured)
				fputs(RESET, stdout);
			r++;
		}
	}

    return EXIT_SUCCESS;
}

