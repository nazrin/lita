PROGRAM = lita

CC = cc
CFLAGS = -O3 -pipe -march=native -Wall
# CFLAGS += -g
LFLAGS =
DIR = src

SRC=$(wildcard $(DIR)/*.c)
OBJ=$(SRC:.c=.o)

$(PROGRAM): $(OBJ)
	$(CC) $(OBJ) $(LFLAGS) -o $(PROGRAM)

%.o: %.c config.h
	$(CC) -c $(CFLAGS) $< -o $@

.PHONY: clean
clean:
	rm -fv -- $(OBJ) $(PROGRAM)

